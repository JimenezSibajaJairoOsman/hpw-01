


	/*un profesor esta compuesto por una "clave","nombre" completo de prof
	#la clave del "departamento" al que pertenece y los "grupos" en los cuales imparte clases

	#un "grupo" esta compuesto por una "clave", un "nombre", un "aula" y aun "horario" y una coleccion de "alumnos"

	#un "alumno " esta compuesto por un numero de control y su nombre completo
	#un "departamento" esta compuesto por una "clave" y su "nombre" */

	
	profesor={
	"clave":"ABC",
	"nombre":"Jairo Jimenez Sibaja",
	"departamento":
	{
		"clave":"1234",
		"nombre":"Sistemas"
	},
	"grupo":
	[
		{
			"clave":"G1",
			"nombre":"I10",
			"horario":"10-11",
			"alumno":[
					{
						"nControl":"09161259",
						"nombre":"Omar Salinas"	
					},
					{
						"nControl":"09161254",
						"nombre":"Pedro Torres"	
					}
				]		
		},
		
		{
			"clave":"G2",
			"nombre":"I11",
			"horario":"11-12",
			"alumno":[
					{
						"nControl":"09161259",
						"nombre":"Felipe Porras"	
					},
					{
						"nControl":"09161254",
						"nombre":"OtroAlumno"	
					}
				]
		}
	]


};
